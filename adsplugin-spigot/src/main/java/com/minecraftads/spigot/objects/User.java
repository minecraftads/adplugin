package com.minecraftads.spigot.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type User.
 */
public class User {


    @SerializedName("uniqueID")
    @Expose
    private String uniqueID;



    @SerializedName("username")
    @Expose
    private String username;

    /**
     * Instantiates a new User.
     *
     * @param uniqueID the unique id
     * @param username the username
     */
    public User(String uniqueID, String username) {
        this.uniqueID = uniqueID;
        this.username = username;
    }

    /**
     * Gets unique id.
     *
     * @return the unique id
     */
    public String getUniqueID() {
        return uniqueID;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }
}
