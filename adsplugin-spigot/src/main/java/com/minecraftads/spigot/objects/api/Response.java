package com.minecraftads.spigot.objects.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Response.
 */
public class Response {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("error_msg")
    @Expose
    private String error_msg;

    private boolean success;

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Gets error msg.
     *
     * @return the error msg
     */
    public String getError_msg() {
        return error_msg;
    }

    /**
     * Is success boolean.
     *
     * @return the boolean
     */
    public boolean isSuccess() {
        if(status.equals("true")) {
            return true;
        }
        return false;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Sets error msg.
     *
     * @param error_msg the error msg
     */
    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
