package com.minecraftads.spigot.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Server.
 */
public class Server {

    /**
     * The Name.
     */
    @SerializedName("name")
    @Expose
    String name;

    /**
     * The Ip.
     */
    @SerializedName("ip")
    @Expose
    String ip;

    /**
     * The Port.
     */
    @SerializedName("port")
    @Expose
    String port;

    /**
     * The Hash.
     */
    @SerializedName("hash")
    @Expose
    String hash;

    /**
     * Instantiates a new Server.
     *
     * @param port the port
     * @param hash the hash
     */
    public Server(String port, String hash) {
        this.port = port;
        this.hash = hash;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets ip.
     *
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets ip.
     *
     * @param ip the ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Gets port.
     *
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * Sets port.
     *
     * @param port the port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * Gets hash.
     *
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * Sets hash.
     *
     * @param hash the hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }
}
