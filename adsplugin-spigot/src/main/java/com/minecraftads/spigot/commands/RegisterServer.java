package com.minecraftads.spigot.commands;

import com.minecraftads.spigot.objects.Server;
import com.minecraftads.spigot.objects.api.Response;
import com.minecraftads.spigot.retrofit.executors.ServerExecutor;
import com.minecraftads.spigot.utils.ConfigUtils;
import com.minecraftads.spigot.utils.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * The type Register server.
 */
public class RegisterServer implements CommandExecutor {
    /**
     * Executor of the /registerserver command.
     */
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(commandSender instanceof Player) {
            commandSender.sendMessage("§4Erreur, cette commande ne peut être exécutée que dans la console !");
            return false;
        }

        ConfigUtils configUtils = new ConfigUtils();


        if(configUtils.getConfigString("config.hash") != null) {
            commandSender.sendMessage("§4Erreur, un hash a déjà été défini ! Supprimez l'objet <hash> de la configuration et réessayez !");
            return false;
        }

        ServerExecutor executor = new ServerExecutor();


        String newHash = StringUtils.getRandomString(32);


        Response response = executor.registerServer(new Server(String.valueOf(commandSender.getServer().getPort()), newHash));

        if(!response.isSuccess()) {
            commandSender.sendMessage("§4Il y a eu une erreur durant l'enregistrement du serveur :");
            commandSender.sendMessage(response.getError_msg());
            return false;
        }

        commandSender.sendMessage("Succès ! Le hash a été sauvegardé. Le voici, ne le divulguez à personne : \n" + newHash);
        configUtils.setConfigString("config.hash", newHash);


        return true;
    }
}
