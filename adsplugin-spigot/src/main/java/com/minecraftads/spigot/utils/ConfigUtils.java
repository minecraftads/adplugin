package com.minecraftads.spigot.utils;

import com.minecraftads.spigot.AdsPlugin;

/**
 * The type Config utils.
 */
public class ConfigUtils {

    /**
     * Gets config string.
     *
     * @param location the location
     * @return the config string
     */
    public String getConfigString(String location) {
        return AdsPlugin.getAdsPlugin().getConfig().getString(location);
    }

    /**
     * Sets config string.
     *
     * @param location the location
     * @param toSet    the string to set in the location
     */
    public void setConfigString(String location, String toSet) {
        AdsPlugin.getAdsPlugin().getConfig().set(location, toSet);
    }

    /**
     * Gets hash.
     *
     * @return the hash
     */
    public String getHash() {
        return getConfigString("config.hash");
    }

    /**
     * Has hash boolean.
     *
     * @return the boolean
     */
    public boolean hasHash() {
        if(getConfigString("config.hash") == null) {
            return false;
        }
        return true;
    }

}
