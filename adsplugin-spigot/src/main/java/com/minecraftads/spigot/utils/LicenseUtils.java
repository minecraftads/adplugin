package com.minecraftads.spigot.utils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

/**
 * The type License utils.
 */
public class LicenseUtils {
    private String licenseKey;
    private Plugin plugin;
    private String validationServer;
    private LogType logType = LogType.NORMAL;
    private String securityKey = "YecoF0I6M05thxLeokoHuW8iUhTdIUInjkfF";
    private boolean debug = false;

    /**
     * Instantiates a new License utils.
     *
     * @param licenseKey       the license key
     * @param validationServer the validation server
     * @param plugin           the plugin
     */
    public LicenseUtils(String licenseKey, String validationServer, Plugin plugin){
        this.licenseKey = licenseKey;
        this.plugin = plugin;
        this.validationServer = validationServer;
    }

    /**
     * Set security key license utils.
     *
     * @param securityKey the security key
     * @return the license utils
     */
    public LicenseUtils setSecurityKey(String securityKey){
        this.securityKey = securityKey;
        return this;
    }

    /**
     * Set console log license utils.
     *
     * @param logType the log type
     * @return the license utils
     */
    public LicenseUtils setConsoleLog(LogType logType){
        this.logType = logType;
        return this;
    }

    /**
     * Debug license utils.
     *
     * @return the license utils
     */
    public LicenseUtils debug(){
        debug = true;
        return this;
    }

    /**
     * Register boolean.
     *
     * @return the boolean
     */
    public boolean register(){
        log(0, "[]==========[License-System]==========[]");
        log(0, "Connectiion au serveur de license....");
        ValidationType vt = isValid();
        if(vt == ValidationType.VALID){
            log(1, "License valide!");
            log(0, "[]==========[License-System]==========[]");
            return true;
        }else{
            log(1, "License invalide");
            log(1, "Cause : "+vt.toString());
            log(1, "Désactivation du plugin !");
            log(0, "[]==========[License-System]==========[]");


            Bukkit.getScheduler().cancelTasks(plugin);
            Bukkit.getPluginManager().disablePlugin(plugin);
            return false;
        }
    }

    /**
     * Is valid simple boolean.
     *
     * @return the boolean
     */
    public boolean isValidSimple(){
        return (isValid() == ValidationType.VALID);
    }

    private String requestServer(String v1, String v2) throws IOException {
        URL url = new URL(validationServer+"?v1="+v1+"&v2="+v2+"&pl="+plugin.getName());
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = con.getResponseCode();
        if(debug){
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
        }

        try(BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            return response.toString();
        }
    }

    /**
     * Is valid validation type.
     *
     * @return the validation type
     */
    public ValidationType isValid(){
        String rand = toBinary(UUID.randomUUID().toString());
        String sKey = toBinary(securityKey);
        String key  = toBinary(licenseKey);

        try {
            String response = requestServer(xor(rand, sKey), xor(rand, key));

            try{
                return ValidationType.valueOf(response);
            }catch(IllegalArgumentException exc){
                String respRand = xor(xor(response, key), sKey);
                if(rand.substring(0, respRand.length()).equals(respRand)) return ValidationType.VALID;
                else return ValidationType.WRONG_RESPONSE;
            }
        } catch (IOException e) {
            if(debug) e.printStackTrace();
            return ValidationType.PAGE_ERROR;
        }
    }


    //
    // Cryptographic
    //

    private static String xor(String s1, String s2){
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < (Math.min(s1.length(), s2.length())) ; i++)
            result.append(Byte.parseByte(""+s1.charAt(i))^Byte.parseByte(s2.charAt(i)+""));
        return result.toString();
    }

    //
    // Enums
    //

    /**
     * The enum Log type.
     */
    public enum LogType{
        /**
         * Normal log type.
         */
        NORMAL,
        /**
         * Low log type.
         */
        LOW,
        /**
         * None log type.
         */
        NONE;
    }

    /**
     * The enum Validation type.
     */
    public enum ValidationType{
        /**
         * Wrong response validation type.
         */
        WRONG_RESPONSE,
        /**
         * Page error validation type.
         */
        PAGE_ERROR,
        /**
         * Url error validation type.
         */
        URL_ERROR,
        /**
         * Key outdated validation type.
         */
        KEY_OUTDATED,
        /**
         * Key not found validation type.
         */
        KEY_NOT_FOUND,
        /**
         * Not valid ip validation type.
         */
        NOT_VALID_IP,
        /**
         * Invalid plugin validation type.
         */
        INVALID_PLUGIN,
        /**
         * Valid validation type.
         */
        VALID;
    }

    //
    // Binary methods
    //

    private String toBinary(String s){
        byte[] bytes = s.getBytes();
        StringBuilder binary = new StringBuilder();
        for (byte b : bytes)
        {
            int val = b;
            for (int i = 0; i < 8; i++)
            {
                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        return binary.toString();
    }

    //
    // Console-Log
    //

    private void log(int type, String message){
        if(logType == LogType.NONE || ( logType == LogType.LOW && type == 0 )) return;
        System.out.println(message);
    }
}
