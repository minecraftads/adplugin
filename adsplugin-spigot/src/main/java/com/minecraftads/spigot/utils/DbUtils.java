package com.minecraftads.spigot.utils;

import com.minecraftads.spigot.AdsPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.*;

/**
 * The type Db utils.
 */
public class DbUtils {

    private static Connection connection;
    private static Statement statement;

    /**
     * Connect.
     */
    public static void connect() {
        connection = null;
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            File file = new File(AdsPlugin.getAdsPlugin().getDataFolder(), "dbLite.db");
            if(!file.exists()) {
                file.createNewFile();
            }

            String url = "jdbc:sqlite:" + file.getPath();
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();
            onUpdate("CREATE TABLE IF NOT EXISTS players (\n"
                    + " uuid varchar(255)\n"
                    + ");");
            AdsPlugin.getAdsPlugin().getLog().info("La base de données est chargée.");
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Disconnect.
     */
    public static void disconnect() {
        if(connection != null) {
            try {
                connection.close();
                System.out.println("La base de données est déconnectée.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * On update.
     *
     * @param str the str
     */
    public static void onUpdate(String str) {
        try {
            statement.execute(str);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * On query result set.
     *
     * @param str the str
     * @return the result set
     */
    public static ResultSet onQuery(String str) {

        try {
            return statement.executeQuery(str);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Has account boolean.
     *
     * @param uuid the uuid
     * @return the boolean
     */
    public static boolean hasAccount(String uuid) {

        try {
            PreparedStatement p = connection.prepareStatement("SELECT uuid FROM players WHERE uuid = ?");
            p.setString(1, uuid);
            ResultSet r = p.executeQuery();
            boolean hasAccount = r.next();
            p.close();
            return hasAccount;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    /**
     * Create account.
     *
     * @param uuid the uuid
     */
    public static void createAccount(String uuid) {
        try{
            PreparedStatement p = connection.prepareStatement("INSERT INTO players(uuid) VALUES(?)");
            p.setString(1, uuid);
            p.execute();
            p.close();
        }catch (SQLException e) {

        }
    }

}
