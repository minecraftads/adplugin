package com.minecraftads.spigot;

import com.minecraftads.spigot.commands.RegisterServer;
import com.minecraftads.spigot.listeners.PlayerListener;
import com.minecraftads.spigot.retrofit.RetrofitAPI;
import com.minecraftads.spigot.utils.DbUtils;
import com.minecraftads.spigot.utils.LicenseUtils;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * The type Ads plugin.
 */
public class AdsPlugin extends JavaPlugin {


    private Logger log;
    private static AdsPlugin adsPlugin;
    private RetrofitAPI retrofitAPI = new RetrofitAPI();

    /**
     * Starts the plugin and loads all it needs.
     */
    @Override
    public void onEnable() {
        log = getLogger();
        adsPlugin = this;
        log.info("AdsPlugin chargé correctement.");
        retrofitAPI.initRetrofit();
        saveDefaultConfig();


        DbUtils.connect();
        getCommand("registerserver").setExecutor(new RegisterServer());
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

    }

    @Override
    public void onDisable() {
        DbUtils.disconnect();
    }

    /**
     * Gets log.
     *
     * @return the log
     */
    public Logger getLog() {
        return log;
    }

    /**
     * Gets ads plugin.
     *
     * @return the ads plugin
     */
    public static AdsPlugin getAdsPlugin() {
        return adsPlugin;
    }
}
