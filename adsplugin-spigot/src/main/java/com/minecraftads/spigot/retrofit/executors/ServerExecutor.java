package com.minecraftads.spigot.retrofit.executors;

import com.minecraftads.spigot.objects.Server;
import com.minecraftads.spigot.objects.User;
import com.minecraftads.spigot.objects.api.Response;
import com.minecraftads.spigot.retrofit.RetrofitAPI;
import com.minecraftads.spigot.retrofit.interfaces.ServerInterface;
import retrofit2.Call;
import retrofit2.Callback;

import java.io.IOException;

/**
 * The type Server executor.
 */
public class ServerExecutor {

    /**
     * The Retrofit api.
     */
    RetrofitAPI retrofitAPI = new RetrofitAPI();

    /**
     * Register server response.
     *
     * @param server the server
     * @return the response
     */
    public Response registerServer(Server server) {
        final Response response = new Response();

        ServerInterface serverInterface = retrofitAPI.getRetrofit().create(ServerInterface.class);

        Call<Response> call = serverInterface.registerServer(server.getHash(), server.getPort());

        try {
            return call.execute().body();
        } catch (IOException e) {
            //e.printStackTrace();
            response.setStatus("false");
            response.setError_msg(e.getMessage());
        }

        return response;
    }

    /**
     * Register user response.
     *
     * @param server the server
     * @param user   the user
     * @return the response
     */
    public Response registerUser(Server server, User user) {
        final Response response = new Response();

        ServerInterface serverInterface = retrofitAPI.getRetrofit().create(ServerInterface.class);

        Call<Response> call = serverInterface.registerUser(server.getHash(), server.getPort(), user.getUniqueID(), user.getUsername());

        try {
            return call.execute().body();
        } catch (IOException e) {
            //e.printStackTrace();
            response.setStatus("false");
            response.setError_msg(e.getMessage());
        }

        return response;
    }

}
