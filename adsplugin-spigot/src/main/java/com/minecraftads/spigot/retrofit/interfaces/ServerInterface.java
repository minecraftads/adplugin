package com.minecraftads.spigot.retrofit.interfaces;

import com.minecraftads.spigot.objects.api.Response;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * The interface Server interface.
 */
public interface ServerInterface {

    /**
     * Register server call.
     *
     * @param hash the hash
     * @param port the port
     * @return the call
     */
    @FormUrlEncoded
    @POST("/link/create_serv")
    Call<Response> registerServer(@Field("hash") String hash, @Field("port") String port);

    /**
     * Register user call.
     *
     * @param hash     the hash
     * @param port     the port
     * @param uniqueID the unique id
     * @param username the username
     * @return the call
     */
    @FormUrlEncoded
    @POST("/link/player_connect")
    Call<Response> registerUser(@Field("hash") String hash, @Field("port") String port, @Field("uniqueID") String uniqueID, @Field("username") String username);


}
