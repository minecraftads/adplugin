package com.minecraftads.spigot.retrofit;

import com.minecraftads.spigot.utils.ConfigUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The type Retrofit api.
 */
public class RetrofitAPI {


    /**
     * The Config utils.
     */
    public ConfigUtils configUtils = new ConfigUtils();
    private Retrofit retrofit;

    /**
     * Init retrofit.
     */
    public void initRetrofit() {
        String baseURL = configUtils.getConfigString("server.host");
        retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    /**
     * Gets retrofit.
     *
     * @return the retrofit
     */
    public Retrofit getRetrofit() {
        initRetrofit();
        return retrofit;
    }


}
