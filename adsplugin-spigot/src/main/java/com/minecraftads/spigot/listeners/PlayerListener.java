package com.minecraftads.spigot.listeners;

import com.minecraftads.spigot.AdsPlugin;
import com.minecraftads.spigot.objects.Server;
import com.minecraftads.spigot.objects.User;
import com.minecraftads.spigot.objects.api.Response;
import com.minecraftads.spigot.retrofit.executors.ServerExecutor;
import com.minecraftads.spigot.utils.ConfigUtils;
import com.minecraftads.spigot.utils.DbUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * The type Player listener.
 */
public class PlayerListener implements Listener {

    /**
     * On player join.
     * Kicks the player if the server isn't registered.
     * Register the user at his first connection.
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        ServerExecutor executor = new ServerExecutor();
        ConfigUtils configUtils = new ConfigUtils();


        if(!configUtils.hasHash()) {
            e.getPlayer().kickPlayer("§4[AdsPlugin] Erreur, les administrateurs du serveur n'ont pas enregistré le serveur.");
            AdsPlugin.getAdsPlugin().getLog().warning("Erreur, aucun hash défini. Enregistrez votre serveur avec : /registerserver");
            return;
        }

        Response response = executor.registerUser(new Server(String.valueOf(e.getPlayer().getServer().getPort()), configUtils.getHash()), new User(e.getPlayer().getUniqueId().toString(), e.getPlayer().getName()));

        if(!response.isSuccess()){
            e.getPlayer().kickPlayer("§4[AdsPlugin] Erreur : " + response.getError_msg());
            AdsPlugin.getAdsPlugin().getLog().warning("Il y a eu une erreur : " + response.getError_msg());
            return;
        }

        if(!DbUtils.hasAccount(e.getPlayer().getUniqueId().toString())) {
            DbUtils.createAccount(e.getPlayer().getUniqueId().toString());
        }
    }

}
